// Copyright (c) 2016, Vladislav Gordievskiy. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:vexflow_angular/vexflow_angular.dart';
import 'package:angular/angular.dart';
import 'dart:html';
import 'dart:async';

@Component(
  selector: 'app-start',
  template: '''
    <div style="display:flex">
      <vexflow margin=15 style="width: 100%; height: 100%;"></vexflow>
    </div>
  ''',
  directives: const [VexflowAngular]
)
class StartPoint implements OnInit, AfterViewInit
{
  @ViewChild(VexflowAngular) VexflowAngular vexflow;

  int y = 0;

  @override
  ngOnInit() {}

  @override
  ngAfterViewInit() {
    vexflow.onReady.then((_) {});
  }
}

main() {
  bootstrap(StartPoint);
}
