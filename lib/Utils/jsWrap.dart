@JS()
library utils.js.wrap;
import 'package:js/js.dart';

@JS('JSON.stringify')
external String stringify(obj);

@JS('JSON.parse')
external String jsfy(obj);