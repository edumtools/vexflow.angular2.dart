import 'package:js/js.dart';

@anonymous
@JS()
class Style {
  external String get strokeStyle;
  external String get fillStyle;
  external String get lineWidth;
  external factory Style({
    String strokeStyle,
    String fillStyle,
    String lineWidth
  });
}