library vexflow_angular.services.data;
import 'dart:async';
import 'package:vexflow/vexflow.dart' as Vex;
import 'package:angular/angular.dart';

import 'package:vexflow_angular/Models/Instrument.dart';
import 'package:tuple/tuple.dart';

@Injectable()
class DataService {
  EventEmitter instrument = new EventEmitter();
  EventEmitter stave = new EventEmitter();

  Map<String, Instrument> _instruments = {};
  String _currentInstrName;
  Iterable<String> get instruments => _instruments.keys;

  bool hasInstrument(String name) => _instruments.containsKey(name);
  Instrument getInstrument(String name) => _instruments[name];
  setCurrentInstrument(String name) {
    _currentInstrName = name;
    instrument.emit(name);
  }
  Instrument getCurrentInstrument() {
    final String name = _currentInstrName != null
      ? _currentInstrName : instruments.first;
    return _instruments[name];
  }

  Instrument addInstrument(String name, StaveParams params,
                           {bool setAsDefault: true})
  {
    Instrument el = Instrument.createInstrument(params);
    el.name = name;
    _instruments[name] = el;
    if(setAsDefault) {
      setCurrentInstrument(name);
    }
    return el;
  }
}
