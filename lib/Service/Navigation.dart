library vexflow_angular.services.navigation;
import 'dart:async';
import 'package:angular/angular.dart';

import 'package:vexflow_angular/Models/PositionData.dart';
export 'package:vexflow_angular/Models/PositionData.dart';

@Injectable()
class NavigationService {
  EventEmitter<TPositionData> changePosition = new EventEmitter<TPositionData>();
  EventEmitter<TPositionData> hightlightPosition = new EventEmitter<TPositionData>();

  final Map<String, TPositionData> _positions = {};
  String _lastSelected = '';

  NavigationService() {}

  setHightlight(TPositionData pos)
  {
    hightlightPosition.emit(pos);
  }

  setPosition(TPositionData pos) {
    _positions[pos.name] = pos;
    _lastSelected = pos.name;
    changePosition.emit(pos);
  }

  int getPositionBy(String name) => _positions[name].position;
  TPositionData getSelectedPosition() => _positions[_lastSelected];
}
