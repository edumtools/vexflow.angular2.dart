// GENERATED CODE - DO NOT MODIFY BY HAND

part of vexflow_angular.models.modifiers.container;

// **************************************************************************
// Generator: BuiltValueGenerator
// Target: abstract class ModifierContainer
// **************************************************************************

class _$ModifierContainer extends ModifierContainer {
  @override
  final String Id;
  @override
  final Map<String, dynamic> Data;
  @override
  final Optional<TClef> clef;
  @override
  final Optional<TKeySignature> keySignature;
  @override
  final Optional<TTimeSig> timeSig;

  factory _$ModifierContainer([updates(ModifierContainerBuilder b)]) =>
      (new ModifierContainerBuilder()..update(updates)).build();

  _$ModifierContainer._(
      {this.Id, this.Data, this.clef, this.keySignature, this.timeSig})
      : super._() {
    if (Id == null) throw new ArgumentError.notNull('Id');
    if (Data == null) throw new ArgumentError.notNull('Data');
    if (clef == null) throw new ArgumentError.notNull('clef');
    if (keySignature == null) throw new ArgumentError.notNull('keySignature');
    if (timeSig == null) throw new ArgumentError.notNull('timeSig');
  }

  @override
  ModifierContainer rebuild(updates(ModifierContainerBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  _$ModifierContainerBuilder toBuilder() =>
      new _$ModifierContainerBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (other is! ModifierContainer) return false;
    return Id == other.Id &&
        Data == other.Data &&
        clef == other.clef &&
        keySignature == other.keySignature &&
        timeSig == other.timeSig;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, Id.hashCode), Data.hashCode), clef.hashCode),
            keySignature.hashCode),
        timeSig.hashCode));
  }

  @override
  String toString() {
    return 'ModifierContainer {'
        'Id=${Id.toString()},\n'
        'Data=${Data.toString()},\n'
        'clef=${clef.toString()},\n'
        'keySignature=${keySignature.toString()},\n'
        'timeSig=${timeSig.toString()},\n'
        '}';
  }
}

class _$ModifierContainerBuilder extends ModifierContainerBuilder {
  ModifierContainer _$v;

  @override
  String get Id {
    _$this;
    return super.Id;
  }

  @override
  set Id(String Id) {
    _$this;
    super.Id = Id;
  }

  @override
  Map<String, dynamic> get Data {
    _$this;
    return super.Data;
  }

  @override
  set Data(Map<String, dynamic> Data) {
    _$this;
    super.Data = Data;
  }

  @override
  Optional<TClef> get clef {
    _$this;
    return super.clef;
  }

  @override
  set clef(Optional<TClef> clef) {
    _$this;
    super.clef = clef;
  }

  @override
  Optional<TKeySignature> get keySignature {
    _$this;
    return super.keySignature;
  }

  @override
  set keySignature(Optional<TKeySignature> keySignature) {
    _$this;
    super.keySignature = keySignature;
  }

  @override
  Optional<TTimeSig> get timeSig {
    _$this;
    return super.timeSig;
  }

  @override
  set timeSig(Optional<TTimeSig> timeSig) {
    _$this;
    super.timeSig = timeSig;
  }

  _$ModifierContainerBuilder() : super._();

  ModifierContainerBuilder get _$this {
    if (_$v != null) {
      super.Id = _$v.Id;
      super.Data = _$v.Data;
      super.clef = _$v.clef;
      super.keySignature = _$v.keySignature;
      super.timeSig = _$v.timeSig;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ModifierContainer other) {
    _$v = other;
  }

  @override
  void update(updates(ModifierContainerBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  ModifierContainer build() {
    final result = _$v ??
        new _$ModifierContainer._(
            Id: Id,
            Data: Data,
            clef: clef,
            keySignature: keySignature,
            timeSig: timeSig);
    replace(result);
    return result;
  }
}
