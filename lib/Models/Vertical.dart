library vexflow_angular.models.vertical;

import 'dart:collection';

import 'StaveItem.dart';

import 'package:js/js.dart';
import 'package:vexflow_angular/Utils/Style.dart';

enum TieType {
  START, END, TIE_NODE, NONE
}

enum VDirection {
  UP, DOWN
}

class NoteItem {
  String pitch;
  String alter;
  bool withDot;
  TieType tieType;
  int linePosition;
  NoteItem(this.pitch, this.linePosition,
          {this.alter: '', this.withDot: false, this.tieType: TieType.NONE });

  String getPitch() => pitch.split('/')[0][0].toLowerCase();
  String getOctave() => pitch.split('/')[1][0].toLowerCase();
}

class Vertical implements IStaveItem {
  static final UnmodifiableListView _pitchs =
    new UnmodifiableListView(['c', 'd', 'e', 'f', 'g', 'a', 'b']);
    static final UnmodifiableListView _pitchsLines =
      new UnmodifiableListView([0, 0.5, 1, 1.5, 2, 2.5, 3]);
  static $ind(String el) => _pitchs.indexOf(el);
  static $line(String el) => _pitchsLines[$ind(el)];

  @override String Id;
  @override Map<String, dynamic> Data;

  List<NoteItem> notes = [];
  String duration;
  VDirection direction;
  bool withDot;
  bool draft = false;

  Style style;
  Vertical(this.Id, this.notes, this.duration,
          {this.Data: const {},
           this.draft: false,
           this.withDot: false,
           this.direction: VDirection.UP});

  prepare() {
    notes.sort((NoteItem a, NoteItem b){
      int indA = $ind(a.getPitch()); int indB = $ind(b.getPitch());
      int octA = int.parse(a.getOctave()); int octB = int.parse(b.getOctave());
      return (indA + _pitchs.length * octA) - (indB + _pitchs.length * octB);
    });
  }

  int expandOffset() => 1;

  setStyle(Style style) {
    this.style = style;
  }

  bool get hasStyle => this.style != null;
}
