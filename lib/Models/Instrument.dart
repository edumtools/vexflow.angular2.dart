library vexflow_angular.models.instrument;
import 'StaveItem.dart';
import 'StaveParams.dart';

export 'StaveParams.dart';
export 'StaveItem.dart';

class Instrument {
  static createInstrument(StaveParams params) {
    Instrument ret =  new Instrument()
      ..params = params;
    return ret;
  }

  String name;
  StaveParams params;
  List<IStaveItem> _verticals = [];

  int get lastIndx => _verticals.length - 1;
  int get lengthVert => _verticals.length;

  setParams(StaveParams params) => this.params = params;

  Iterable<IStaveItem> get musicVerticals => _verticals;
  int addVertical(IStaveItem el) {
    _verticals.add(el);
    return lastIndx;
  }

  addAllVerticals(List<IStaveItem> items) => _verticals.addAll(items);

  insertVertical(int ind, IStaveItem el) {
    if (ind < _verticals.length) {
      _verticals.insert(ind, el);
    }
  }

  changeVertical(int ind, IStaveItem el){
    if (ind < _verticals.length) {
      _verticals[ind] = el;
    } else {
      addVertical(el);
    }
  }

  removeVertical(int indx) {
    if (_verticals.length > indx) {
      _verticals.removeAt(indx);
    }
  }

  int removeLastVertical() {
    if(_verticals.isNotEmpty) {
      _verticals.removeLast();
    }
    return lastIndx;
  }

  clearAll() {
    _verticals.clear();
  }
}
