library vexflow_angular.stave.params;

import 'package:vexflow_angular/Utils/Style.dart';


/* StaveClef:
   'treble': { line_shift: 0 },
   'bass': { line_shift: 6 },
   'tenor': { line_shift: 4 },
   'alto': { line_shift: 3 },
   'soprano': { line_shift: 1 },
   'percussion': { line_shift: 0 },
   'mezzo-soprano': { line_shift: 2 },
   'baritone-c': { line_shift: 5 },
   'baritone-f': { line_shift: 5 },
   'subbass': { line_shift: 7 },
   'french': { line_shift: -1 }
 */
/* StaveAnnotation
 * 8va
 * 8vb
 */
class StaveParams {
  static const int VexflowResolution = 16384;
  int x;
  int y;
  int widthOfNote = 10;
  int width;
  int line_width;
  int spacing_between_lines_px;
  int space_above_staff_ln = 4;
  int space_below_staff_ln = 4;
  int num_lines = 5;
  String lineFillStyle;
  String type;
  String size;
  String annotation;
  String timeSignature;
  String keySignature;

  Style style;

  int get baseTicks {
    if (timeSignature?.isEmpty ?? true) return 0;
    return num_beats * (VexflowResolution / beat_value).truncate();
  }

  int get num_beats {
    if (timeSignature?.isEmpty ?? true) return 1;
    return int.parse(timeSignature?.split('/')[0], onError: (_) => 1);
  }
  int get beat_value {
    if (timeSignature?.isEmpty ?? true) return 1;
    return int.parse(timeSignature?.split('/')[1], onError: (_) => 1);
  }

  StaveParams clone() {
    return new StaveParams()
      ..x = x
      ..y = y
      ..style = style
      ..lineFillStyle = lineFillStyle
      ..widthOfNote = widthOfNote
      ..width = width
      ..num_lines = num_lines
      ..line_width = line_width
      ..spacing_between_lines_px = spacing_between_lines_px
      ..space_below_staff_ln = space_below_staff_ln
      ..type = type
      ..size = size
      ..annotation = annotation
      ..keySignature = keySignature
      ..timeSignature = timeSignature
      ..space_above_staff_ln = space_above_staff_ln;
  }
}
