library vexflow_angular.models.position_data;
import 'package:quiver_optional/optional.dart';
import 'package:vexflow/vexflow.dart' as Vex;

class TPositionData {
  final String name;
  final int position;
  final Optional<Vex.Tickable> el;
  final Optional<String> id;
  final Optional<Map> data;
  TPositionData(this.name, this.position,
    {Vex.Tickable el, String id, String type, Map data})
    : this.el = el == null ? const Optional.absent() : new Optional.of(el)
    , this.id = id == null ? const Optional.absent() : new Optional.of(id)
    , this.data = data == null ? const Optional.absent() : new Optional.of(data);
}