library vexflow_angular.models.rest;

import 'Vertical.dart';

class Rest extends Vertical {
  static const String baseRestPitch = 'a';
  
  @override String  get Id => super.Id;

  Rest(String Id, int octave, String duration,
    {Map data: const {}, String restPitch: baseRestPitch,  int linePosition: 5})
      : super(Id, [new NoteItem("$restPitch/$octave", linePosition)],
          "${duration}r", Data: data);
}
