library vexflow_angular.models.time_sig;

import 'StaveItem.dart';

class TTimeSig implements IStaveItem {
  @override String Id;
  @override Map<String, dynamic> Data;
  
  String value;
  TTimeSig(this.Id, this.value, {this.Data: const {}});

  int get baseTicks {
    if (value?.isEmpty ?? true) return 0;
    return 1024;
  }

  int get num_beats {
    if (value?.isEmpty ?? true) return 1;
    return int.parse(value?.split('/')[0]);
  }
  int get beat_value {
    if (value?.isEmpty ?? true) return 1;
    return int.parse(value?.split('/')[1]);
  }
  
  @override
  int expandOffset() => 1;
}