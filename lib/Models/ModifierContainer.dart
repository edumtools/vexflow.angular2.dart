library vexflow_angular.models.modifiers.container;

import 'package:built_value/built_value.dart';
import 'package:quiver_optional/optional.dart';
import 'package:vexflow_angular/Models/StaveItem.dart';

import 'Clef.dart';
import 'KeySignature.dart';
import 'TimeSig.dart';

part 'ModifierContainer.g.dart';

bool _isNull(el) => el == null;

abstract class ModifierContainer 
  implements Built<ModifierContainer, ModifierContainerBuilder>,
             IStaveItem
{
  Type get runtimeType => ModifierContainer;
  factory ModifierContainer([updates(ModifierContainerBuilder b)])
    = _$ModifierContainer;
  ModifierContainer._();

  String get Id;
  Map<String, dynamic> get Data;

  Optional<TClef> get clef;
  Optional<TKeySignature> get keySignature;
  Optional<TTimeSig> get timeSig;

  int expandOffset() {
    return (clef.isPresent ? 1 : 0) + (timeSig.isPresent ? 1 : 0)
      + (keySignature.isPresent ? 1 : 0);
  }
}

abstract class ModifierContainerBuilder
  implements Builder<ModifierContainer, ModifierContainerBuilder>
{
  Type get runtimeType => ModifierContainerBuilder;
  ModifierContainerBuilder._();
  factory ModifierContainerBuilder() = _$ModifierContainerBuilder;

  String Id;
  String AppType;
  Map<String, dynamic> Data;

  Optional<TClef> clef;
  Optional<TKeySignature> keySignature;
  Optional<TTimeSig> timeSig;

  create(String id,{TClef clef, TKeySignature keySig, TTimeSig timeSig,
    Map data: const {}})
  {
    _qa() => const Optional.absent();
    this.Id = id;
    this.Data = data;
    this.clef = _isNull(clef) ? _qa() : new Optional.of(clef);
    this.keySignature = _isNull(keySig) ? _qa() : new Optional.of(keySig);
    this.timeSig = _isNull(timeSig) ? _qa() : new Optional.of(timeSig);
  }
}