library vexflow_angular.models.key_signature;

import 'StaveItem.dart';

class TKeySignature implements IStaveItem {
  @override String Id;
  @override Map<String, dynamic> Data;

  String value;
  TKeySignature(this.Id, {this.value: '', this.Data: const {}});

  @override
  int expandOffset() => 1;
}
