library vexflow_angular.models.clef;

import 'StaveItem.dart';

class TClef implements IStaveItem {
  @override String Id;
  @override Map<String, dynamic> Data;

  String value;
  String size;
  bool asNote;
  TClef(this.Id, this.value,
    {this.size: '', this.asNote: false, this.Data: const {}});

  TClef clone()
  {
    return new TClef(this.Id, this.value, size: this.size, asNote: this.asNote, Data: this.Data);
  }
}
