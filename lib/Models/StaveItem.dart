library vexflow_angular.models.stave_item;

import 'package:meta/meta.dart';

abstract class IStaveItem {
  @virtual String get Id;
  @virtual Map<String, dynamic> get Data;
  @virtual int expandOffset() => 1;
  @virtual setStyle(obj);
}
