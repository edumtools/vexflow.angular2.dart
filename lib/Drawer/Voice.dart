library vexflow_angular.drawer.voice;
import 'dart:async';

import 'package:logging/logging.dart';
import 'package:vexflow/vexflow.dart' as Vex;
import 'package:quiver_optional/optional.dart' as q;
import 'package:vexflow_angular/Models/StaveItem.dart';
import 'package:vexflow_angular/Models/StaveParams.dart';
import 'package:vexflow_angular/Service/DataService.dart';

import 'package:vexflow_angular/Models/Vertical.dart';
import 'package:vexflow_angular/Models/Rest.dart';
import 'package:vexflow_angular/Models/Clef.dart';
import 'package:vexflow_angular/Models/KeySignature.dart';
import 'package:vexflow_angular/Models/TimeSig.dart';
import 'package:vexflow_angular/Models/ModifierContainer.dart';

import 'IDrawer.dart';
import 'StaveItemsDrawers/StaveItemsCommon.dart';
import 'StaveItemsDrawers/ChordDrawer.dart';
import 'StaveItemsDrawers/ClefDrawer.dart';
import 'StaveItemsDrawers/KeySignatureDrawer.dart';
import 'StaveItemsDrawers/TimeSignatureDrawer.dart';
import 'StaveItemsDrawers/ModifierContainerDrawer.dart';
import 'Models/StaveItemWrap.dart';
import 'Tie.dart';

export 'IDrawer.dart';
export 'Models/StaveItemWrap.dart';

class VoiceDrawerData {
  Vex.Voice voice;
  Vex.Stave target;
  Iterable<IStaveItem> verticals;
  Iterable<IStaveItem> origin;
  List<StaveItemWrapper> result;
  TClef clef;
  int widthOfNote;
  final int renderedItems;
  int baseTick;
  int num_beats;
  int beat_value;
  int get totalTicksPerMeasure => baseTick;
  VoiceDrawerData({
     this.voice, this.target, this.verticals, this.origin, this.renderedItems,
     this.widthOfNote, this.baseTick, this.num_beats, this.beat_value, this.clef
  });
}

class VoiceDrawer implements IDrawer<VoiceDrawerData> {
  static final _log = new Logger('VoiceDrawer');
  static const List<String> tickableFilter = const ["BarNote", "GhostNote"];
  static const spaceAround = 15;
  static bool isNote(Vex.Tickable el) {
    final String type = el.getAttribute('type');
    return !tickableFilter.contains(type);
  }

  static bool isNear(Vex.Tickable el, int x, int y, int treshold) {
    final int diff = (getXForTickable(el) - x).abs();
    return diff <= treshold;
  }

  static int getXForTickable(Vex.Tickable el) {
    switch (el.getAttribute('type')) {
      case "StaveNote" : {
        return (el as Vex.StaveNote).getNoteHeadBeginX();
      }
      case "TimeSigNote":
      case "ClefNote" : {
        Vex.Note clef = el as Vex.Note;
        int x = clef.getX();
        x = x.isNaN ? 0 : x; 
        return x + clef.getWidth() + clef.getWidth() / 2;
      } break;
      case "KeySignatureNote" : {
        Vex.Note keySig = el as Vex.Note;
        int x = keySig.getAbsoluteX();
        x = x.isNaN ? 0 : x; 
        return x + (keySig.getWidth() / 2).truncate();
      }break;
    }
    return 0;
  }

  static q.Optional<Vex.Tickable> getPreferForPnt(
    Vex.Tickable lha, Vex.Tickable rha, int x, int y, {int treshold: 50})
  {
    final int lhaX = getXForTickable(lha);
    final int rhaX = getXForTickable(rha);
    final int lhaDiff = (lhaX - x).abs();
    final int rhaDiff = (rhaX - x).abs();
    if (lhaDiff >= treshold && rhaDiff >= treshold) {
      return new q.Optional.absent();
    }
    return lhaDiff >= rhaDiff ? new q.Optional.of(rha) : new q.Optional.of(lha);
  }

  static bool isBelongPos(Vex.Tickable el, int x, int y, {int width: 0}) {
    if (el is Vex.StaveNote) {
      Vex.BoundingBox box = el.getBoundingBox();
      final int centerX = el.getNoteHeadBeginX();
      if (width == 0) {
        width = (el.getWidth() / 2).round();
      }
      final int leftEdge = centerX - width;
      final int rightEdge = centerX + width;
      return x >= leftEdge && x <= rightEdge;
    } else {
      return false;
    }
  }

  static Iterable<StaveItemWrapper<IStaveItem>> toItemWithWidth(
    Iterable<IStaveItem> origin, Vex.Stave stave,
    {IDataDraverProvider drawerData,
     int totalTicksPerMeasure: 0,
     bool withFakeClef: false,
     int onWidth: 0,
     TClef clef})
  {
    List<StaveItemWrapper> items =
      VoiceDrawer.prepare(origin, null, totalTicksPerMeasure,
        clef: clef,
        drawerData: drawerData,
        withFakeClef: withFakeClef
      )
        .toList(growable: false);

    Vex.Voice voice = new Vex.Voice(new Vex.VoiceParams(num_beats: 1,
      beat_value: 1))
        ..setStrict(false)
        ..setStave(stave);

    items.forEach((StaveItemWrapper el) {
        voice.addTickable(el.vexItem);
        (el.vexItem as Vex.Note).setStave(stave);
    });

    new Vex.Formatter().joinVoices([voice]).format([voice], onWidth, null);
    int idx = 0;
    voice.getTickables()
      .forEach((Vex.Tickable el)
    {
      final int extraLeft = (el as Vex.Note).getExtraLeftPx();
      final int extraRight = (el as Vex.Note).getExtraRightPx();
      final int elWidth = el.getTickContext().getWidth() + extraLeft + extraRight;
      items[idx].width = elWidth;
      idx += 1;
    });
    return items;
  }

  static Vex.Voice getVoice(int num_beats, int beat_value) =>
    new Vex.Voice(new Vex.VoiceParams(
      num_beats: num_beats, beat_value: beat_value))
        ..setStrict(false);

  static  Vex.Voice createVoiceFromStave(StaveParams params)
      => VoiceDrawer.getVoice(params.num_beats, params.beat_value);

  static VoiceDrawerData createDataFromStave(Iterable<IStaveItem> forDraw,
    Iterable<IStaveItem> origin, int renderedItems, Vex.Stave stave, StaveParams params,
    {TClef clef})
    => new VoiceDrawerData(
      voice: createVoiceFromStave(params),
      target: stave,
      verticals: forDraw,
      origin: origin,
      renderedItems: renderedItems,
      widthOfNote: params.widthOfNote,
      baseTick: params.baseTicks,
      num_beats: params.num_beats,
      beat_value: params.beat_value,
      clef: clef);

  final DataService data;
  final IDataDraverProvider drawerData;
  VoiceDrawer(this.data, this.drawerData);

  int getStaveWidth(int notes, int widthOfNote, Vex.Stave stave)
    => widthOfNote * notes - drawerData.getMargin();

  static int getOriginStaveWidth(IDataDraverProvider drawerData)
    => drawerData != null
        ? drawerData.getStaveWidth() - drawerData.getMargin()
        : 0;

  int getResultWidth(List<StaveItemWrapper> result,
                     Iterable<IStaveItem> verticals,
                     Iterable<IStaveItem> origin,
                     int renderedItems, int widthOfNote, Vex.Stave target)
  {
    final int length = verticals.length;
    final int width = getStaveWidth(length, widthOfNote, target);
    final int staveWidth = getOriginStaveWidth(drawerData);
    final int forDraw = origin.length - renderedItems;
    if (width < staveWidth && result.isNotEmpty) {
      if (result.last.isBarNote && forDraw > 0) {
        return staveWidth;
      }
    }
    return width;
  }

  Iterable<StaveItemWrapper> checkEndBarline(Iterable<StaveItemWrapper> result,
    Vex.Stave stave, { IDataDraverProvider drawerData, int resultWidth: 0})
  {
    final int staveWidth = getOriginStaveWidth(drawerData);
    if (result.isNotEmpty
       && result.last.isBarNote
       && (staveWidth - resultWidth) <= 2 * drawerData.getWidthOfNote())
    {
      stave.setEndBarType(Vex.BarlineType.SINGLE);
      stave.draw();
      return result.take(result.length - 1);
    }
    return result;
  } 

  /*--Stave Item Drawers and expander*/
  static Map<Type, TStaveItemExpander> _expanders = {
    Vertical : ChordDrawer.expand,
    Rest : ChordDrawer.expand,
    ModifierContainer: ModifierContainerDrawer.expand
  };

  static Map<Type, IDrawer> _drawers = {
    Vertical : new ChordDrawer(),
    TClef : new ClefDrawer(),
    TKeySignature: new KeySignatureDrawer(),
    TTimeSig: new TimeSignatureDrawer()
  };

  static Iterable<StaveItemWrapper> prepare(Iterable<IStaveItem> items,
    Vex.Voice voice, int totalTicksPerMeasure,
    {
      IDataDraverProvider drawerData,
      int widthOfNote: 1,
      Iterable<IStaveItem> origin: const [],
      int renderedItems: 0,
      TClef clef,
      bool withFakeClef: false
    })
  {
    final int staveWidth = getOriginStaveWidth(drawerData);
    final int maxItems = (staveWidth / widthOfNote).truncate();
    final int forDraw = origin.length - renderedItems;
    int sumTicks = 0;

    int idx = 0;

    return items.expand((IStaveItem el) {
      List<StaveItemWrapper<IStaveItem>> ret = [];

      if (withFakeClef && idx == 0)
      {
        TClef fakeClef = clef != null ? clef.clone() : new TClef('fake', 'treble');
        fakeClef.Id = 'fake';

        if ((el is ModifierContainer))
        {
          if (!el.clef.isPresent)
          {
            ret.add(ClefDrawer.getWrapper(
              clef != null ? clef : new TClef('fake', 'treble'),
              new TItemParams(glyphFontScale: drawerData.getGlyphFontScale()))
            );
            ModifierContainerDrawer.addGhostEndNote(ret);
          }
        }
        else
        {
            ret.add(ClefDrawer.getWrapper(
              clef != null ? clef : new TClef('fake', 'treble'),
              new TItemParams(glyphFontScale: drawerData.getGlyphFontScale()))
            );
            ModifierContainerDrawer.addGhostEndNote(ret);
        }
      }

      idx += 1;

      if (_expanders.containsKey(el.runtimeType)) {
        TStaveItemExpander expander = _expanders[el.runtimeType];
        Iterable<StaveItemWrapper<IStaveItem>> expandedSymbol = expander(el, drawerData, items, clef: clef,
          staveWidth: staveWidth, maxItems: maxItems, forDraw: forDraw,
          sumTicks: sumTicks, totalTicksPerMeasure: totalTicksPerMeasure,
          tickSetter: new q.Optional.of((int val) => sumTicks = val))
            .map((StaveItemWrapper item)
          {
            item.vexItem.setAttribute(IDrawer.Id, el.Id);
            if (item.vexItem.getAttribute(IDrawer.Data) == null) {
              item.vexItem.setAttribute(IDrawer.Data,
                el.Data.isNotEmpty ? el.Data : null);
            }
            return item;
          });

          ret.addAll(expandedSymbol);
      }

      return ret;
    });
  }

  _drawItem(StaveItemWrapper<IStaveItem> el, VoiceDrawerData data) {
    if (el.drawTrhoughtDrawer) {
      final Type type = el.item.runtimeType;
      if (_drawers.containsKey(type)) {
        TItemDrawerData itemData = new TItemDrawerData(el,
          stave: new q.Optional.of(data.target));
        _drawers[type].draw(data: itemData);
      }
    } else {
      if (el.vexItem is Vex.KeySignatureNote) {
        (el.vexItem as Vex.KeySignatureNote).setStave(data.target);
      }
      data.voice.addTickable(el.vexItem as dynamic);
    }
}

  @override
  Future draw({VoiceDrawerData data: null}) {
    assert(data != null);
    var fmt = new Vex.Formatter();
    Vex.Voice voice = data.voice;
    data.result = VoiceDrawer.toItemWithWidth(data.verticals, data.target,
      clef: data.clef, drawerData: drawerData,
      totalTicksPerMeasure: data.totalTicksPerMeasure)
        .toList(growable: false);

    final int widthByItems = data.result
      .where((StaveItemWrapper el) => VoiceDrawer.isNote(el.vexItem))
      .fold(0, (int ret, StaveItemWrapper el)
        => ret + (el.width < drawerData.getWidthOfNote()
          ? drawerData.getWidthOfNote() : el.width));

    final int realWidth = widthByItems > data.target.getWidth()
      ? data.target.getWidth() - drawerData.getWidthOfNote(): widthByItems;

    data.result = checkEndBarline(data.result, data.target,
      drawerData: drawerData,
      resultWidth: realWidth);
    
    data.result.where((StaveItemWrapper<IStaveItem> el) => el.needDraw)
      .forEach((StaveItemWrapper<IStaveItem> el) => _drawItem(el, data));

    fmt.joinVoices([voice])
      .format([voice], realWidth, null);
    voice.draw(drawerData.getRenderContext(), data.target);

    new TieDrawer(this.data, drawerData)
      ..draw(data: data.result.where((el)=>!el.isBarNote).toList());
    return new Future.value();
  }
}
