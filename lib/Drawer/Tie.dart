library vexflow_angular.drawer.tie;
import 'dart:async';
import 'package:vexflow/vexflow.dart' as Vex;
import "package:logging/logging.dart";
import 'package:vexflow_angular/Service/DataService.dart';
import 'package:vexflow_angular/Models/Vertical.dart';
import 'Voice.dart' show StaveItemWrapper;

import 'IDrawer.dart';
export 'IDrawer.dart';


class TieDrawer implements IDrawer<List<StaveItemWrapper>> {
  static final _log = new Logger('TieDrawer');
  final DataService data;
  final IDataDraverProvider drawerData;
  TieDrawer(this.data, this.drawerData);

  _toChain(List ret, StaveItemWrapper el)
  {
    if (el.item is Vertical) {
      Vertical item = el.item;
      if (item.notes.any((el) => el.tieType == TieType.TIE_NODE))
      {
        (ret.last as List).add(el);
      }
      if (item.notes.any((el) => el.tieType == TieType.START))
      {
        ret.add([el]);
      }
    }
    return ret;
  }

  @override
  Future draw({List<StaveItemWrapper> data: null}) {
    assert(data != null);
    $node(el) => el.tieType == TieType.TIE_NODE || el.tieType == TieType.START;
    $notes(StaveItemWrapper el) => (el.item as Vertical).notes;
    List<List<StaveItemWrapper>> items =
      data.where((el) => el.item is Vertical)
        .where((el)=>$notes(el).any($node)).fold([], _toChain);

    for(List<StaveItemWrapper> chain in items) {
      Iterator<StaveItemWrapper> startIt = chain.iterator;
      Iterator<StaveItemWrapper> endIt = chain.skip(1).iterator;
      while(startIt.moveNext() && endIt.moveNext()) {
        StaveItemWrapper start = startIt.current;
        StaveItemWrapper end = endIt.current;
        {
          Iterable<NoteItem> starts = (start.item as Vertical).notes.where($node);
          Iterable<NoteItem> ends = (end.item as Vertical).notes.where($node);

          Iterator<int> endsIndx =
            ends.map((el) => $notes(end).indexOf(el))
            .iterator;
          Iterator<int> startsIndx = starts
            .where((el) => ends.any((elEnd)=>el.getPitch() == elEnd.getPitch()))
            .map((el) => $notes(start).indexOf(el))
            .iterator;

          while(endsIndx.moveNext() && startsIndx.moveNext()) {
            Vex.StaveTie tie = new Vex.StaveTie(new Vex.StaveTieParams(
              first_note: start.vexItem,
              last_note: end.vexItem,
              first_indices: [startsIndx.current],
              last_indices: [endsIndx.current]
            ));
            tie.setContext(drawerData.getRenderContext());
            tie.draw();
          }
        }
      }
    }

    return new Future.value();
  }
}
