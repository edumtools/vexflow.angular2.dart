@JS()
library vexflow_angular.drawer.stave;
import 'dart:async';
import 'package:js/js.dart';
import 'package:vexflow/vexflow.dart' as Vex;
import 'package:vexflow_angular/Models/StaveParams.dart';
import 'package:vexflow_angular/Service/DataService.dart';

import 'IDrawer.dart';
export 'IDrawer.dart';

/*
 vertical_bar_width: 10,       // Width around vertical bar end-marker
 glyph_spacing_px: 10,
 num_lines: 5,
 fill_style: '#999999',
 left_bar: true,               // draw vertical bar on left
 right_bar: true,               // draw vertical bar on right
 spacing_between_lines_px: 10, // in pixels
 space_above_staff_ln: 4,      // in staff lines
 space_below_staff_ln: 4,      // in staff lines
 top_text_position: 1,          // in sta
 */

@JS()
@anonymous
class StaveConfig {
  external bool get left_bar;
  external bool get right_bar;
  external int get num_lines;
  external String get fill_style;
  external int get line_width;
  external int get glyph_spacing_px;
  external int get spacing_between_lines_px;
  external int get vertical_bar_width;
  external int get space_above_staff_ln;
  external int get space_below_staff_ln;
  external factory StaveConfig({
    bool left_bar,
    bool right_bar,
    int num_lines,
    String fill_style,
    int line_width,
    int spacing_between_lines_px,
    int vertical_bar_width,
    int glyph_spacing_px,
    int space_above_staff_ln,
    int space_below_staff_ln
  });
}

class StaveDrawer implements IDrawer<Vex.Stave> {
  static Vex.Stave create(StaveParams params, {bool isEmpty: true}) {
    final bool withClef = params.type != null && params.type.isNotEmpty;
    StaveConfig conf = new StaveConfig(
      left_bar: withClef, right_bar: false,
      num_lines: params.num_lines,
      fill_style: params.lineFillStyle,
      line_width: params.line_width,
      spacing_between_lines_px: params.spacing_between_lines_px,
      space_below_staff_ln: params.space_below_staff_ln,
      space_above_staff_ln: params.space_above_staff_ln
    );

    Vex.Stave ret = new Vex.Stave(params.x, params.y, params.width, conf);
    if (params.style != null) {
      (ret as Vex.ElementBase).setStyle(params.style);
    }
    if (params.annotation != null && params.size != null) {
      ret.addClef(params.type, params.size, params.annotation);
    } else if (params.type != null && params.type.isNotEmpty) {
      ret.addClef(params.type);
    }
    if (params.timeSignature?.isNotEmpty ?? false) {
      ret.addTimeSignature(params.timeSignature, 0);
    }

    if (params.keySignature != null) {
      ret.addKeySignature(params.keySignature);
    }
    return ret;
  }

  final DataService data;
  final IDataDraverProvider drawerData;
  StaveDrawer(this.data, this.drawerData);

  @override
  Future draw({Vex.Stave data: null}) {
    assert(data != null);
    data.setContext(drawerData.getRenderContext());
    data.setWidth(drawerData.getStaveWidth());
    if (drawerData.getNoteShift() != 0) {
      data.setNoteStartX(drawerData.getNoteShift());
    }
    data.draw();
    return new Future.value();
  }
}
