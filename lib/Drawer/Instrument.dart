library vexflow_angular.drawer.instrument;
import 'dart:async';
import 'package:logging/logging.dart';
import 'package:quiver_optional/optional.dart' as q;
import 'package:tuple/tuple.dart';
import 'package:vexflow/vexflow.dart' as Vex;
import 'package:vexflow_angular/Models/Vertical.dart';
import 'package:vexflow_angular/Models/Clef.dart';
import 'package:vexflow_angular/Models/Instrument.dart';
import 'package:vexflow_angular/Models/KeySignature.dart';
import 'package:vexflow_angular/Models/ModifierContainer.dart';
import 'package:vexflow_angular/Models/PositionData.dart';
import 'package:vexflow_angular/Models/TimeSig.dart';
import 'package:vexflow_angular/Service/DataService.dart';
import 'package:vexflow_angular/Service/Navigation.dart';

import 'package:vexflow_angular/Models/PositionData.dart';

import 'IDrawer.dart';
export 'IDrawer.dart';
export 'package:tuple/tuple.dart';

import 'Stave.dart';
import 'Voice.dart';
import 'Cursor.dart';
import 'StaveItemsDrawers/ChordDrawer.dart';
import 'StaveItemsDrawers/ModifierContainerDrawer.dart';

class TInstrumentDrawData {
  static final _log = new Logger('TInstrumentDrawData');
  Instrument _instr;
  StaveParams params;
  Vex.Stave stave;
  CursorDrawData cursorData;
  List<Vex.Stave> allStaves;
  int staveInd;
  int _renderetNotes = 0;
  bool _isDone = false;
  int get renderedNotes => _renderetNotes;
  Instrument get $instr => _instr;
  String get name => $instr.name;
  bool get isDone => _isDone;
  int allChords = 0;
  TInstrumentDrawData(this._instr, {this.params, this.stave, this.cursorData,
    this.allStaves, this.staveInd: 0})
  {
    allChords = _instr.musicVerticals.length;
  }

  int getNotHandledVerticals() => $instr.musicVerticals.length - renderedNotes;
  setStave(Vex.Stave stave) => this.stave = stave;
  setStaveInd(int ind) => staveInd = ind;
  setCursorData(CursorDrawData cursor) => cursorData = cursor;
  addRenderedNotes(int notes) {
    _renderetNotes += notes;
    _log.info("add rendered note: ${notes} -> ${renderedNotes}");
  }
  done() {
      _log.info("${$instr.name} is done");
    _isDone = true;
  }
}

class TRenderedItem {
  List<Tuple2<Vex.Stave, Vex.Voice>> items = [];
  TRenderedItem();
  add(Vex.Stave stave, Vex.Voice voice) {
    items.add(new Tuple2(stave, voice));
  }

  List<Vex.Tickable> get tickables => items.map((el) => el.item2)
    .fold([], (List ret, Vex.Voice voice){
      if(voice == null) return ret;
      ret.addAll(voice.getTickables()
        .where((el) => VoiceDrawer.isNote(el))
        .toList(growable: true));
      return ret;
    });
}

class InstrumentsDrawer implements IDrawer<dynamic> {
  static final _log = new Logger('InstrumentsDrawer');
  final DataService dataProvider;
  final NavigationService navigation;
  final IDataDraverProvider drawerData;
  final Map<String, TRenderedItem> renderedData = {};
  final Map<String, CursorDrawData> cursorData = {};
  bool disableCursor = false;
  final CursorDrawer cursorDrawer;
  InstrumentsDrawer(this.dataProvider, this.navigation, this.drawerData)
    : cursorDrawer = new CursorDrawer(dataProvider, navigation, drawerData);

  bool inStave(Vex.Stave item, int x, int y) {
      Vex.BoundingBox box = item.getBoundingBox();
      return y >= box.getY() && y <= (box.getY() + box.getH());
  }

  TPositionData byIndex(int idx, String name) {
    final TRenderedItem rendData = renderedData[name];
    Vex.Tickable el = rendData.tickables[idx];
    final String uniqueId = el.getAttribute(IDrawer.Id);
    final Map data = el.getAttribute(IDrawer.Data);
    return new TPositionData(name, idx, el: el, id: uniqueId, data: data);
  }

  TPositionData inPosition(int x, int y)
  {
    for (String name in renderedData.keys) {
      final TRenderedItem rendData = renderedData[name];
      final Tuple2<Vex.Stave, Vex.Voice> item = rendData.items
        .firstWhere((item) => inStave(item.item1, x, y), orElse: () => null);
      if (item != null) {
          final int treshold = drawerData.getWidthOfNote() * 2;
          Vex.Tickable el = item?.item2?.getTickables()
            ?.where(VoiceDrawer.isNote)
            ?.map((el) => el as Vex.Tickable)
            ?.where((el) => VoiceDrawer.isNear(el, x, y, treshold))
            ?.fold(null, (Vex.Tickable prev, Vex.Tickable el){
              if (prev == null) return el;
              q.Optional<Vex.Tickable> pref = VoiceDrawer
                .getPreferForPnt(prev, el, x, y,
                  treshold: drawerData.getWidthOfNote());
              return pref.or(el);
            });
          if (el != null) {
            var elToIndx = el;
            List tickables = rendData.tickables;
            final String uniqueId = el.getAttribute(IDrawer.Id);
            final Map data = el.getAttribute(IDrawer.Data);
            return new TPositionData(name, tickables.indexOf(elToIndx),
              el: el, id: uniqueId, data: data);
          } else {
            final int position = rendData.tickables.isNotEmpty
                                   ? rendData.tickables.length : 0;
            return new TPositionData(name, position);
          }
      }
    }
    return null;
  }

  drawStave(Vex.Stave stave) {
      StaveDrawer drawer = new StaveDrawer(dataProvider, drawerData);
      drawer.draw(data: stave);
  }

  setDisableCursor(bool val) => disableCursor = val;

  drawCursor(CursorDrawData data) {
    if (!disableCursor) {
      cursorDrawer.draw(data: data);
    } else {
      cursorDrawer.clearPrev();
      cursorDrawer.stopFlicker();
    }
  }

  drawHightlight(CursorDrawData data, TPosition pos) {
    cursorDrawer.drawHightlight(pos, data: data);
  }

  Iterable<StaveItemWrapper> getMaxItemsOnStave(Iterable<IStaveItem> origin,
    Vex.Stave stave, {int totalTicksPerMeasure: 0})
  {
    final maxItems = (stave.getWidth() / drawerData.getWidthOfNote()).floor();
    final possibleWidth = drawerData.getWidthOfNote() * maxItems;

    int realWidth = 0;
    
    Iterable<StaveItemWrapper> items = VoiceDrawer.toItemWithWidth(
      origin.take(maxItems), stave, 
      drawerData: drawerData, totalTicksPerMeasure: totalTicksPerMeasure, withFakeClef: true);
    
    return items.takeWhile((StaveItemWrapper el) {
      int len = realWidth + (el.width < drawerData.getWidthOfNote()
        ? drawerData.getWidthOfNote()
        : el.width);
      if (len < possibleWidth) {
        realWidth = len;
        return true;
      } else {
        return false;
      }
    });
  }

  Iterable<IStaveItem> getVerticalsForDraw(
    Iterable<IStaveItem> origin, TInstrumentDrawData instData)
  {
    Iterable<IStaveItem> base = origin.skip(instData.renderedNotes);
    VoiceDrawerData voiceArg =
      VoiceDrawer.createDataFromStave([], [], 0, instData.stave,
        instData.params);

    int maxItems = (instData.stave.getWidth() / drawerData.getWidthOfNote()).floor();
    final possibleWidth = drawerData.getWidthOfNote() * maxItems;
    double realWidth = 0.0;

    /* The case when it is not the first stave*/
    if (base.length > 0 && base.first is Vertical)
    {
      maxItems -= 1;
    }

    maxItems = maxItems >= 0 ? maxItems : 0;

    List<StaveItemWrapper<IStaveItem>> items = VoiceDrawer.toItemWithWidth(
      base.take(maxItems),
      instData.stave, 
      drawerData: drawerData,
      totalTicksPerMeasure: voiceArg.totalTicksPerMeasure,
      withFakeClef: true,
      onWidth: possibleWidth
    )
    .takeWhile((StaveItemWrapper el) {
      Vex.Tickable vexEl = el.vexItem;
      final String type = vexEl.getAttribute('type');
      double width = el.width.toDouble();

      switch (type)
      {
        case 'StaveNote' :
        {
          width = drawerData.getWidthOfNote().toDouble();
          break;
        }
        case 'BarNote' :
        {
          width = 2.0;
          break;
        }
      }

      double len = realWidth + width;
      //print("!!! ${type} - ${width} | ${len} | ${possibleWidth}");
      if (len < possibleWidth) {
        realWidth = len;
        return true;
      } else {
        return false;
      }
    }).toList();

    List<String> tickableFilter = const ["BarNote", "GhostNote", "TimeSigNote"];

    Function isOriginItem = (StaveItemWrapper<IStaveItem> el) {
        Vex.Tickable vexEl = el.vexItem;
        final String type = el.vexItem.getAttribute('type');
        return el.item?.Id != 'fake' && !tickableFilter.contains(type);
    };

    int totalElements = items.where(isOriginItem).length;
    int size = totalElements;

    if (base.length > totalElements)
    {
      bool isStartOfList = false;
      int backOffset = items.reversed.takeWhile((el) {
          Vex.Tickable vexEl = el.vexItem;
          final String type = vexEl.getAttribute('type');
          isStartOfList = ['GhostNote', 'ClefNote'].contains(type);
          return VoiceDrawer.isNote(vexEl);
      }).length;

      if (isStartOfList)
      {
        size = totalElements;
      }
      else
      {
        size = totalElements - backOffset;
      }
    }
    else
    {
      size = base.length;
    }

    return base.take(size);
  }

  num getStaveHeight(Vex.Stave stave)
  {
    return stave.getYForBottomText(12) - stave.getYForBottomText(0);
  }

  checkDrawerSize(Iterable<Vex.Stave> items) {
    final double scale = drawerData.getScale();

    final int totalHeight = (items.fold(0, (int res, Vex.Stave stave)
      => res += getStaveHeight(stave)) * scale + (getStaveHeight(items.first) / 2).ceil());

    drawerData.setStaveHeight(totalHeight);
    if (drawerData.getAllStavesHeight() < totalHeight) {
      drawerData.resize(height: totalHeight);
      CursorDrawer.moveScreenOnStave(items.last, drawerData);
    }
  }

  postHandleData(Instrument instrument, Vex.Stave stave, Vex.Voice voice) {
    final String key = instrument.name;
    if(!renderedData.containsKey(key)) {
      renderedData[key] = new TRenderedItem();
    }
    renderedData[key].add(stave, voice);
  }

  Iterable<IStaveItem> drawNotesOnStave(TInstrumentDrawData drawData)
  {
    Instrument instrument = drawData.$instr;
    Iterable<IStaveItem> forDraw =
      getVerticalsForDraw(instrument.musicVerticals, drawData);
    drawData.addRenderedNotes(forDraw.length);

    if(forDraw.isNotEmpty) {
      drawStave(drawData.stave);
      q.Optional<TClef> clef = haveClef(instrument);
      VoiceDrawerData arg =
        VoiceDrawer.createDataFromStave(forDraw, instrument.musicVerticals,
                                        drawData.renderedNotes,
                                        drawData.stave, drawData.params,
                                        clef: clef.orNull);
      VoiceDrawer drawer = new VoiceDrawer(dataProvider, this.drawerData);
      drawer.draw(data: arg);
      drawData.allStaves.add(drawData.stave);
      postHandleData(instrument, drawData.stave, arg.voice);
      drawData.cursorData.addStave(drawData.stave, arg.voice);
    } else {
      drawStave(drawData.stave);
      drawData.allStaves.add(drawData.stave);
      drawData.cursorData.addStave(drawData.stave,
        VoiceDrawer.createVoiceFromStave(drawData.params));
      postHandleData(instrument, drawData.stave, null);
    }

    if (!drawerData.disableCheckSize()) {
      checkDrawerSize(drawData.allStaves);
    }
    
    return forDraw;
  }

  drawConnectors(Iterable<Vex.Stave> staves) {
    final _log = new Logger('TConnectorDrawer');
    drawConnectorByType(Vex.Stave top, Vex.Stave bottom, int type) {
      Vex.StaveConnector connector = new Vex.StaveConnector(top, bottom);
      connector.setType(type);
      connector.setContext(drawerData.getRenderContext());
      connector.draw();
    }
    Iterator<Vex.Stave> top = staves.iterator;
    Iterator<Vex.Stave> bottom = staves.skip(1).iterator;
    while(top.moveNext() && bottom.moveNext()) {
      var topSt = top.current;
      var bottomSt = bottom.current;
      drawConnectorByType(topSt, bottomSt, Vex.StaveConnector.SINGLE_LEFT);
      drawConnectorByType(topSt, bottomSt, Vex.StaveConnector.SINGLE_RIGHT);
    }
  }

  setTimeSigFromModifier(ModifierContainer firstModifier, StaveParams params) {
    q.Optional<ModifierContainer> val = new q.Optional
      .fromNullable(firstModifier);
    
    val.ifPresent((ModifierContainer el){
      el.timeSig.ifPresent((TTimeSig timeSig) {
        params.timeSignature = timeSig.value;
      });
    });

    return params;
  }

  setFirstBarline(Vex.Stave stave)
  {
    stave.setBegBarType(Vex.BarlineType.SINGLE);
  }


  q.Optional<TClef> haveClef(Instrument el) {
    ModifierContainer item = el.musicVerticals
      .firstWhere((el) => el is ModifierContainer && el.clef.isPresent,
        orElse: () => null);
    if (item != null) {
      return new q.Optional.of(item.clef.value);
    }
    return const q.Optional.absent();
  }

  bool isStop(Iterable<IStaveItem> base, TInstrumentDrawData instData) {
    return instData.renderedNotes == instData.allChords;
  }

  @override
  Future draw({dynamic data: null}) {

    var input = dataProvider.instruments;

    assert(data == null);
    renderedData.clear();
    cursorData.clear();
    List<Vex.Stave> allStaves = [];

    List<TInstrumentDrawData> items = input.map((el){
      Instrument instrument = dataProvider.getInstrument(el);
      StaveParams staveParams = instrument.params.clone();
      
      setTimeSigFromModifier(instrument.musicVerticals
        .firstWhere(
          (el) => el is ModifierContainer,
          orElse: () => null), staveParams);
      
      CursorDrawData cursorDrawData = new CursorDrawData()
        ..instrument = instrument;
      cursorData[el] = cursorDrawData;
      return new TInstrumentDrawData(instrument,
                                     params: staveParams,
                                     allStaves: allStaves,
                                     cursorData: cursorDrawData,
                                     staveInd: 0);
    }).toList();

    int startY = items.length > 0 ? items.first.params.y : 0;

    while(items.any((el) => !el.isDone)) {
      List<Vex.Stave> connectedStave = [];
      for(TInstrumentDrawData el in items) {
        el.params.y = startY + (allStaves.isNotEmpty
          ? allStaves.fold(0, (height, el) => height + getStaveHeight(el))
          : 0);

        q.Optional<TClef> clef = haveClef(el.$instr);

        if(el.staveInd == 0 && !el.isDone) {
          el.setStave(StaveDrawer
            .create(el.params.clone()
              ..timeSignature = null));
          clef.ifPresent((_) => setFirstBarline(el.stave));
        } else {
          el.setStave(StaveDrawer
            .create(el.params.clone()
            ..type = clef.isPresent ? clef.value.value : null
            ..timeSignature = null));
        }

        {/*Draw notes on Stave*/
          var forDraw = drawNotesOnStave(el);

          connectedStave.add(el.stave);

          if(isStop(forDraw, el)) {
            el.done();
          } else {
            el.setStaveInd(el.staveInd + 1);
          }
        }
      }
      drawConnectors(connectedStave);
    }
    drawCursorOnCurrent();
    return new Future.value();
  }

  drawCursorOnCurrent() {
    drawCursor(cursorData[dataProvider.getCurrentInstrument().name]);
  }

  hightlightCursorOnCurrent(TPosition pos) {
    drawHightlight(cursorData[dataProvider.getCurrentInstrument().name], pos);
  }

  clearHightlight()
  {
    cursorDrawer.clearHightlight();
  }
}
