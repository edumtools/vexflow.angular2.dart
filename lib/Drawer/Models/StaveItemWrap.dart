library vexflow_angular.drawer.models.stave_item_wrapper;

import 'package:vexflow/vexflow.dart' as Vex;
import '../../Models/StaveItem.dart';

enum StaveItemType {
  CHROD, BAR, CLEF, KEY_SIGNATURE, TIME_SIGNATURE
}

class StaveItemWrapper<StaveType extends IStaveItem> {
  final StaveItemType type;
  StaveType item;
  Vex.Tickable vexItem;
  int width;
  bool needDraw;
  bool drawTrhoughtDrawer;
  bool get isBarNote => vexItem.getAttribute('type') == "BarNote";
  StaveItemWrapper(this.type, this.item, this.vexItem,
    {this.needDraw: true, this.drawTrhoughtDrawer: false, this.width});
}
