library vexflow_angular.drawer.drawer;
import 'dart:async';

export 'IDataDraverProvider.dart';

abstract class IDrawer<TDrawData> {
  static const String Id = "uniqueId";
  static const String Data = "appData";

  Future draw({TDrawData data: null});
}
