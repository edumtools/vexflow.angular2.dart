library vexflow_angular.drawer.data_drawer_provider;
import 'dart:async';
import 'package:vexflow/vexflow.dart' as Vex;

abstract class IDataDraverProvider {
  int getAllStavesHeight();
  double getScale();
  int getStaveWidth();
  int getWidthOfNote();
  int getNoteShift();
  int getMargin();
  setStaveHeight(int totalHeight);
  String getCursorImage();
  double getGlyphFontScale();
  Future<Null> resize({int width: null, int height: null});
  Future<Null> scrollTo({int position: null});
  Vex.IRenderContext getRenderContext();
  bool disableCheckSize();
}
