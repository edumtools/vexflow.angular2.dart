library vexflow_angular.drawer.clear;
import 'dart:async';
import 'package:vexflow/vexflow.dart' as Vex;

import 'IDrawer.dart';
export 'IDrawer.dart';

class ClearDraver implements IDrawer<dynamic> {
  final Vex.IRenderContext ctx;
  ClearDraver(this.ctx);

  @override
  Future draw({dynamic data: null}) {
    ctx.clear();
    return new Future.value();
  }
}
