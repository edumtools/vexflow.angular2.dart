library vexflow_angular.drawer.cursor;
import 'dart:async';
import 'dart:html';
import 'package:rxdart/rxdart.dart' as rx;
import 'package:quiver_optional/optional.dart' as q;
import 'package:logging/logging.dart';
import 'package:tuple/tuple.dart';
import 'package:vexflow/vexflow.dart' as Vex;
import 'package:vexflow_angular/Models/PositionData.dart';
import 'package:vexflow_angular/Models/Instrument.dart';
import 'package:vexflow_angular/Models/Vertical.dart';
import 'package:vexflow_angular/Service/DataService.dart';
import 'package:vexflow_angular/Service/Navigation.dart';

import 'IDrawer.dart';
export 'IDrawer.dart';

import 'Voice.dart';

class CursorDrawData {
  static final _log = new Logger('CursorDrawData');
  int position;
  Instrument instrument;
  /*Voices represents the Stave
   * first Stave, Second Voice
   */
  List<Tuple2<Vex.Stave, Vex.Voice>> voices = [];

  addStave(Vex.Stave stave, Vex.Voice voice) {
    voices.add(new Tuple2(stave, voice));
  }

  List<Tuple3<Vex.Stave, Vex.Voice, Vex.Tickable>> getAllTickables()
  => voices.fold([], (List ret, Tuple2<Vex.Stave, Vex.Voice> el){
    ret.addAll(el.item2.getTickables()
      .map((Vex.Tickable tickable) => new Tuple3(el.item1, el.item2, tickable)));
    return ret;
  });

  /* first Stave, Second Voice, Third Ticable*/
  List<Tuple3<Vex.Stave, Vex.Voice, Vex.Tickable>> getAllTickableNotes()
    => getAllTickables().where((Tuple3<Vex.Stave, Vex.Voice, Vex.Tickable> el)
      => VoiceDrawer.isNote(el.item3)).toList(growable: false);
}

class CursorDrawer implements IDrawer<CursorDrawData> {
  static final _log = new Logger('CursorDrawer');
  static const int width = 20;

  final DataService dataProvider;
  final NavigationService navigation;
  final IDataDraverProvider drawerData;
  final ImageElement cursor;
  final rx.Observable flicker =
    new rx.Observable.periodic(new Duration(milliseconds: 700));
  StreamSubscription flickerSubs;

  bool _turnOff = false;
  Vex.Stave _prevStave;
  int _prevX = 0; int _prevY = 0;

  CursorDrawer(this.dataProvider, this.navigation, this.drawerData)
    : cursor = new ImageElement(src: drawerData.getCursorImage())
  {
    init();
  }

  init() {
    bool hightlight = false;
    flickerSubs = flicker.listen((_) {
      if (hightlight && _prevStave != null) {
        drawCursor(_prevX, _prevStave);
      } else {
        clearPrev();
      }
      hightlight = !hightlight;
    });
    flickerSubs.pause();
  }

  CanvasRenderingContext2D getCtx() => Vex.getCanvasContext('cursor');
  CanvasRenderingContext2D getHigtlightCtx() => Vex.getCanvasContext('hightlight');

  Vex.BoundingBox getBoxFrom(CursorDrawData data, Vex.BoundingBox orig) {
    Vex.BoundingBox box = Vex.BoundingBox.copy(orig);
    return box;
  }

  startFlicker() {
    _turnOff = false;
    if (flickerSubs.isPaused) {
      flickerSubs.resume();
    }
  }

  stopFlicker() {
    _turnOff = true;
    if (!flickerSubs.isPaused) {
      flickerSubs.pause();
    }
  }

  clearPrev() {
    CanvasRenderingContext2D ctx = getCtx();
    ctx.clearRect(_prevX, _prevY, width, width);
  }

  drawCursor(int x, Vex.Stave stave) {
    try {
      if (_turnOff) {
        clearPrev();
        return;
      }
      CanvasRenderingContext2D ctx = getCtx();
      final int y = stave.getYForLine(2) - (width / 2).truncate();
      ctx.drawImageScaled(cursor, x, y, width, width);
      {
        _prevX = x; _prevY = y;
        _prevStave = stave;
      }
    } catch (err, stack)
    {
      print("${err.toString()} | ${stack.toString()}");
    }
  }

  int _prevHL_X = 0;
  int _prevHL_Y = 0;


  _drawHightlight(int x, Vex.Stave stave) {
    CanvasRenderingContext2D ctx = getHigtlightCtx();
    final num y = stave.getYForLine(0);
    final num endY = stave.getYForLine(4);

    final height = endY - y;

    ctx.clearRect(
      _prevHL_X - 5, 
      _prevHL_Y - 5,
      10 * 2,
      (stave.getHeight() / 2) * 2);

    x += 2;

    ctx.fillStyle = 'rgba(19, 189, 51, 0.4)';
    ctx.fillRect(x, y, 10, height);

    {
      _prevHL_X = x;
      _prevHL_Y = y;
    }
    moveScreenOnStave(stave, drawerData);
  }

  clearHightlight()
  {
    CanvasElement hightlightEl = document.getElementById("hightlight");
    CanvasRenderingContext2D ctx = getHigtlightCtx();
    ctx.clearRect(0, 0, hightlightEl.width, hightlightEl.height);
  }

  static num getStaveHeight(Vex.Stave stave)
  {
    return stave.getYForBottomText(12) - stave.getYForBottomText(0);
  }

  static moveScreenOnStave(Vex.Stave stave, IDataDraverProvider drawerData) {
    /*Try to auto scroll*/
    num staveHeight = getStaveHeight(stave);
    final int offset = staveHeight + (staveHeight / 2).ceil();
    final int y = stave.getYForBottomText(6) - offset;
    drawerData.scrollTo(position: (y * drawerData.getScale()).ceil());
  }

  checkAndStartFlick(int offset) {
    try {
      Vertical item = dataProvider.getCurrentInstrument().musicVerticals
        .toList().elementAt(offset);
      if (item == null) {
        startFlicker();
        return;
      }
      if (item.notes.isEmpty || !item.draft) {
        startFlicker();
      } else {
        stopFlicker();
      }
    } catch (err) {
      startFlicker();
    }
  }

  drawOnClef(Vex.Stave stave) {
    final int width = stave.getNoteStartX() - stave.getX();
    drawCursor(stave.getX() + (width / 2).truncate(), stave);
  }

  Future drawHightlight(TPosition pos, {CursorDrawData data: null}) {
    clearPrev();

    int offset = pos.position;

    try {
      List tickables = data.getAllTickableNotes();
      if (tickables.isEmpty) {
        Vex.Stave stave = data.voices[0].item1;
        _drawHightlight(stave.getNoteStartX(), stave);
      } else if(offset < tickables.length) {
        Tuple3<Vex.Stave, Vex.Voice, Vex.Tickable> target =
          tickables.skip(offset).first;
        _drawHightlight(VoiceDrawer.getXForTickable(target.item3), target.item1);
      } else if (offset == tickables.length) {
        Tuple3<Vex.Stave, Vex.Voice, Vex.Tickable> target = tickables.last;
        if (target.item3 is Vex.StaveNote) {
          final int noteWidth = data.instrument.params.widthOfNote;
          final int x = VoiceDrawer.getXForTickable(target.item3);
          _drawHightlight(x + noteWidth, target.item1);
        }
      }
    } catch (err) {
      _log.severe(err);
    }
    return new Future.value();
  }

  @override
  Future draw({CursorDrawData data: null}) {
    clearPrev();

    int offset = navigation.getSelectedPosition().position;
    checkAndStartFlick(offset);
    if (offset < 0) {
      drawOnClef(data.voices[0].item1);
      return new Future.value();
    }
    try {
      List tickables = data.getAllTickableNotes();
      if (tickables.isEmpty) {
        Vex.Stave stave = data.voices[0].item1;
        drawCursor(stave.getNoteStartX(), stave);
      } else if(offset < tickables.length) {
        Tuple3<Vex.Stave, Vex.Voice, Vex.Tickable> target =
          tickables.skip(offset).first;
        drawCursor(VoiceDrawer.getXForTickable(target.item3), target.item1);
      } else if (offset == tickables.length) {
        Tuple3<Vex.Stave, Vex.Voice, Vex.Tickable> target = tickables.last;
        if (target.item3 is Vex.StaveNote) {
          final int noteWidth = data.instrument.params.widthOfNote;
          final int x = VoiceDrawer.getXForTickable(target.item3);
          drawCursor(x + noteWidth, target.item1);
        }
      }
    } catch (err) {
      _log.severe(err);
    }
    return new Future.value();
  }
}
