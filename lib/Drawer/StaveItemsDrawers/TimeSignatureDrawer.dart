library vexflow_angular.drawer.stave_items_drawers.time_signature;

import 'dart:async';
import 'package:vexflow/vexflow.dart' as Vex;
import 'package:quiver_optional/optional.dart' as q;
import '../../Models/StaveItem.dart';
import '../../Models/TimeSig.dart';
import '../Models/StaveItemWrap.dart';
import '../IDrawer.dart';
import 'StaveItemsCommon.dart';
import 'package:vexflow_angular/Models/Clef.dart';

class TimeSignatureDrawer implements IDrawer<TItemDrawerData<TTimeSig>> {

  static Vex.TimeSigNote createItem(TTimeSig el, [TItemParams params = null])
  {
    final double glyphFontScale = params?.glyphFontScale ?? 30;
    return new Vex.TimeSigNote(el.value)
      ..setAttribute(IDrawer.Data, el.Data.isNotEmpty ? el.Data : null);
  }

  static StaveItemWrapper<TTimeSig> getWrapper(TTimeSig el)
    => new StaveItemWrapper<TTimeSig>(StaveItemType.TIME_SIGNATURE, el, 
        createItem(el), drawTrhoughtDrawer: false);

  static List<StaveItemWrapper<TTimeSig>>
    expand(TTimeSig el, IDataDraverProvider drawerData, Iterable<IStaveItem> items,
          {int staveWidth: 0, int maxItems: 0, int forDraw: 0, int sumTicks: 0,
           int totalTicksPerMeasure: 0,
           q.Optional<TSetTick> tickSetter: const q.Optional<TSetTick>.absent(),
           TClef clef})
  {
    return [getWrapper(el)];
  }

  @override
  Future draw({TItemDrawerData<TTimeSig> data: null}) {
    return new Future.error("not implemented");
  }
}
