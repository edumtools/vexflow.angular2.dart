library vexflow_angular.drawer.stave_items_drawers.clef;

import 'dart:async';
import 'package:vexflow/vexflow.dart' as Vex;
import 'package:quiver_optional/optional.dart' as q;
import '../../Models/StaveItem.dart';
import '../../Models/Clef.dart';
import '../Models/StaveItemWrap.dart';
import '../IDrawer.dart';
import 'StaveItemsCommon.dart';

class ClefDrawer implements IDrawer<TItemDrawerData<TClef>> {

  static Vex.ClefNote createClefNote(TClef el, [TItemParams params = null])
  {
    final double glyphFontScale = params?.glyphFontScale ?? 30;
    var ret = new Vex.ClefNote(el.value)
      ..setAttribute(IDrawer.Data, el.Data.isNotEmpty ? el.Data : null);

    return ret;
  }

  static StaveItemWrapper<TClef> getWrapper(TClef el, [TItemParams params = null])
    => new StaveItemWrapper<TClef>(StaveItemType.CLEF, el, createClefNote(el, params),
        drawTrhoughtDrawer: false);

  static List<StaveItemWrapper<TClef>>
    expand(TClef el, IDataDraverProvider drawerData, Iterable<IStaveItem> items,
          {int staveWidth: 0, int maxItems: 0, int forDraw: 0, int sumTicks: 0,
           int totalTicksPerMeasure: 0,
           q.Optional<TSetTick> tickSetter: const q.Optional<TSetTick>.absent(),
           TClef clef})
  {
    return [getWrapper(el, new TItemParams(glyphFontScale: drawerData.getGlyphFontScale()))];
  }

  @override
  Future draw({TItemDrawerData<TClef> data: null}) {
    if (data.stave.isPresent) {
      TClef clef = data.item.item;
      Vex.Stave stave = data.stave.value;
      stave.addClef(clef.value);
      stave.draw();
    }
    return new Future.value();
  }
}
