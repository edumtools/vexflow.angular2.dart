library vexflow_angular.drawer.stave_items_drawers.modifier_container;

import 'dart:async';
import 'package:vexflow/vexflow.dart' as Vex;
import 'package:quiver_optional/optional.dart' as q;
import '../../Models/ModifierContainer.dart';
import '../../Models/StaveItem.dart';
import '../../Models/Clef.dart';
import '../../Models/KeySignature.dart';
import '../../Models/TimeSig.dart';
import '../Models/StaveItemWrap.dart';
import '../IDrawer.dart';
import 'StaveItemsCommon.dart';
import 'ClefDrawer.dart';
import 'KeySignatureDrawer.dart';
import 'TimeSignatureDrawer.dart';

class ModifierContainerDrawer
  implements IDrawer<TItemDrawerData<ModifierContainer>>
{
  static const String modifierGroupName = "modifier_grp";

  static addGhostEndNote(List list) {
      var gn = new Vex.GhostNote(
        new Vex.StaveNoteOption(keys: ['C4'], duration: 'q'));
      list.add(new StaveItemWrapper(StaveItemType.CHROD, null, gn,
        drawTrhoughtDrawer: false)); 
  }

  static List<StaveItemWrapper<IStaveItem>>
    expand(ModifierContainer el, IDataDraverProvider drawerData,
          Iterable<IStaveItem> items,
          {int staveWidth: 0, int maxItems: 0, int forDraw: 0, int sumTicks: 0,
           int totalTicksPerMeasure: 0,
           q.Optional<TSetTick> tickSetter: const q.Optional<TSetTick>.absent(),
           TClef clef})
  {
    List<StaveItemWrapper<IStaveItem>> ret = [];
    el.clef.ifPresent((TClef el)
      => ret.add(ClefDrawer.getWrapper(el, new TItemParams(glyphFontScale: drawerData.getGlyphFontScale()))));
    el.keySignature.ifPresent((TKeySignature item) {
      String clef = el.clef.orNull?.value;
      ret.add(KeySignatureDrawer.getWrapper(item, clef: clef));
    });
    el.timeSig.ifPresent((TTimeSig el)
      => ret.add(TimeSignatureDrawer.getWrapper(el)));
    addGhostEndNote(ret);
    return ret;
  }

  @override
  Future draw({TItemDrawerData<ModifierContainer> data: null}) {
    return new Future.error("not used");
  }
}
