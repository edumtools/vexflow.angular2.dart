library vexflow_angular.drawer.stave_items_drawers.chord;

import 'dart:async';
import 'package:vexflow/vexflow.dart' as Vex;
import 'package:quiver_optional/optional.dart' as q;
import '../../Models/StaveItem.dart';
import '../../Models/Vertical.dart';
import '../Models/StaveItemWrap.dart';
import '../IDrawer.dart';
import 'StaveItemsCommon.dart';
import 'package:vexflow_angular/Models/Clef.dart';
import 'package:vexflow_angular/Utils/jsWrap.dart';

class ChordDrawer implements IDrawer<TItemDrawerData<Vertical>> {

  static int getOriginStaveWidth(IDataDraverProvider drawerData)
    => drawerData != null
        ? drawerData.getStaveWidth() - drawerData.getMargin()
        : 0;

  static Vex.StaveNote createStaveNote(Vertical el, [TItemParams params = null])
  {
    final int direction = el.direction == VDirection.DOWN
      ? Vex.StaveNote.STEM_DOWN : Vex.StaveNote.STEM_UP;
    final double glyphFontScale = params?.glyphFontScale ?? 30;
    Vex.StaveNoteOption option = new Vex.StaveNoteOption(
      keys: el.notes.map((el) => "${el.pitch}").toList(growable: true),
      duration: el.duration,
      glyph_font_scale: glyphFontScale,
      clef: params.clef
    );
    final ret = new Vex.StaveNote(option)..setStemDirection(direction);
    if (el.hasStyle) {
      ret.setStyle(el.style);
    }
    return ret;
  }

  static Vex.DraftPitch createDraftNote(Vertical el, [TItemParams params = null])
  {
    final String draftNoteDuration = 'q';
    final double glyphFontScale = params?.glyphFontScale ?? 30;
    Vex.StaveNoteOption option = new Vex.StaveNoteOption(
      keys: el.notes.map((el) => "${el.pitch}").toList(growable: true),
      duration: draftNoteDuration,
      glyph_font_scale: glyphFontScale,
      clef: params.clef
    );
    final ret = new Vex.DraftPitch(option);
    if (el.hasStyle) {
      ret.setStyle(el.style);
    }
    return ret;
  }

  static Vex.Note createStaveChordFrom(Vertical el, [TItemParams params = null]) {
    el.prepare();
    Vex.StaveNote ret;
    if(!el.draft) {
      ret = createStaveNote(el, params);
    } else {
      ret = createDraftNote(el, params);
    }

    ret.setXShift(params.shiftX);

    if(el.withDot) {
      ret.addDotToAll();
    }

    for(NoteItem note in el.notes) {
      if(note.alter.isNotEmpty) {
        ret.addAccidental(el.notes.indexOf(note),
          new Vex.Accidental(note.alter));
      }
      if(!el.withDot && note.withDot) {
        ret.addDot(el.notes.indexOf(note));
      }
    }
    return ret;
  }

  static StaveItemWrapper<Vertical> handleItem(Vertical el, 
    IDataDraverProvider drawerData, {TClef clef})
  {
    var noteParams = new TItemParams(
      glyphFontScale: drawerData.getGlyphFontScale(),
      clef: clef?.value ?? 'treble'
    );
    Vex.Note note = createStaveChordFrom(el, noteParams);
    return new StaveItemWrapper<Vertical>(StaveItemType.CHROD, el, note);
  }

  static StaveItemWrapper<Vertical> getBar(Vertical el, bool needDraw)
  {
    return new StaveItemWrapper<Vertical>(StaveItemType.BAR, el,
      new Vex.BarNote(), needDraw: needDraw);
  }

  static List<StaveItemWrapper<Vertical>>
    expand(dynamic el, IDataDraverProvider drawerData, Iterable<IStaveItem> items,
          {int staveWidth: 0, int maxItems: 0, int forDraw: 0, int sumTicks: 0,
           int totalTicksPerMeasure: 0,
           q.Optional<TSetTick> tickSetter: const q.Optional<TSetTick>.absent(),
           TClef clef})
  {
    StaveItemWrapper<Vertical> item = 
      ChordDrawer.handleItem(el, drawerData, clef: clef);
    if (!item.item.draft && tickSetter.isPresent) {
      final int tickValue = item.vexItem.getTicks().value();
      sumTicks = sumTicks + tickValue;
      tickSetter.value(sumTicks);
    }
    if (el.duration == "b" && tickSetter.isPresent) { tickSetter.value(0);}
    if (totalTicksPerMeasure > 0
        && sumTicks == totalTicksPerMeasure
        && tickSetter.isPresent)
    {
      tickSetter.value(0);
      bool drawBar = true;
      if (el == items.last) {
        if (items.length == maxItems || forDraw > 0) {
          drawBar = false;
        }
      }
      return [item, ChordDrawer.getBar(el, drawBar)];
    }
    return [item];
  }

  @override
  Future draw({TItemDrawerData<Vertical> data: null}) {
    // TODO: implement draw
  }
}
