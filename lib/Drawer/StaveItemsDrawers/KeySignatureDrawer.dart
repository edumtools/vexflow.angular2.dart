library vexflow_angular.drawer.stave_items_drawers.key_signature;

import 'dart:async';
import 'package:vexflow/vexflow.dart' as Vex;
import 'package:quiver_optional/optional.dart' as q;
import '../../Models/StaveItem.dart';
import '../../Models/KeySignature.dart';
import '../Models/StaveItemWrap.dart';
import '../IDrawer.dart';
import 'StaveItemsCommon.dart';
import 'package:vexflow_angular/Models/Clef.dart';

class KeySignatureDrawer implements IDrawer<TItemDrawerData<TKeySignature>> {

  static Vex.KeySignatureNote createItem(TKeySignature el,
    {String clef, TItemParams params})
  {
    final double glyphFontScale = params?.glyphFontScale ?? 30;
    return new Vex.KeySignatureNote(el.value, clef)
      ..setAttribute(IDrawer.Data, el.Data.isNotEmpty ? el.Data : null);
  }

  static StaveItemWrapper<TKeySignature> getWrapper(TKeySignature el, {String clef})
    => new StaveItemWrapper<TKeySignature>(StaveItemType.KEY_SIGNATURE, el,
        createItem(el, clef: clef), drawTrhoughtDrawer: false);

  static List<StaveItemWrapper<TKeySignature>>
    expand(TKeySignature el, IDataDraverProvider drawerData, Iterable<IStaveItem> items,
          {int staveWidth: 0, int maxItems: 0, int forDraw: 0, int sumTicks: 0,
           int totalTicksPerMeasure: 0,
           q.Optional<TSetTick> tickSetter: const q.Optional<TSetTick>.absent(),
           TClef clef})
  {
    return [getWrapper(el)];
  }

  @override
  Future draw({TItemDrawerData<TKeySignature> data: null}) {
    return new Future.error("");
  }
}
