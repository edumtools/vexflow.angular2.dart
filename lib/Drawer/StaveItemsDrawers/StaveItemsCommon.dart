library vexflow_angular.drawer.stave_items_drawers.common;

import 'package:vexflow/vexflow.dart' as Vex;
import 'package:quiver_optional/optional.dart' as q;
import '../Models/StaveItemWrap.dart';
import '../../Models/StaveItem.dart';
import '../../Drawer/IDataDraverProvider.dart';
import 'package:vexflow_angular/Models/Clef.dart';

typedef TSetTick(int tick);

class TItemDrawerData<StaveType extends IStaveItem> {
  StaveItemWrapper<StaveType> item;
  q.Optional<Vex.Stave> stave;
  TItemDrawerData(this.item, { this.stave: const q.Optional.absent() });
}

class TItemParams {
  final double glyphFontScale;
  final int shiftX;
  final String clef;
  TItemParams({
    this.glyphFontScale: 39.0,
    this.shiftX: 0,
    this.clef});
}

typedef Iterable<StaveItemWrapper<IStaveItem>> TStaveItemExpander<T>(
  T el, IDataDraverProvider drawerData, Iterable<IStaveItem> items,
  {int staveWidth, int maxItems, int forDraw, int sumTicks,
   int totalTicksPerMeasure, q.Optional<TSetTick> tickSetter,
   TClef clef});
