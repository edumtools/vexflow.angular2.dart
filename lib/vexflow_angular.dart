// Copyright (c) 2016, Vladislav Gordievskiy. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.
library vexflow_angular;
import 'dart:async';
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:logging/logging.dart';
import 'package:quiver_optional/optional.dart' as q;
import 'package:tuple/tuple.dart';
import 'package:vexflow/vexflow.dart' as Vex;
import 'package:rxdart/rxdart.dart' as rx;

import 'Utils/EventEmitter.dart' as Em;
import 'Drawer/AllDrawers.dart' as draw;
import 'Models/Instrument.dart';
import 'Service/DataService.dart';
import 'Service/Navigation.dart';
import 'package:vexflow_angular/Drawer/Stave.dart';
import 'package:vexflow_angular/Drawer/Voice.dart';

export 'package:vexflow/vexflow.dart';

export 'Models/PositionData.dart';
export 'Models/Instrument.dart';
export 'Models/StaveParams.dart';
export 'Models/StaveItem.dart';
export 'Models/Vertical.dart';
export 'Models/Rest.dart';
export 'Models/Clef.dart';
export 'Models/KeySignature.dart';
export 'Models/TimeSig.dart';
export 'Models/ModifierContainer.dart';

@Component(
  selector: 'vexflow',
  styles: const [":host { overflow: auto; }"],
  styleUrls: const ['vexflow_angular.css'],
  templateUrl: 'vexflow_angular.html',
  directives: const [CORE_DIRECTIVES],
  providers: const [DataService, NavigationService],
  changeDetection: ChangeDetectionStrategy.Default
)
class VexflowAngular implements draw.IDataDraverProvider,
  OnInit, OnDestroy, OnChanges, AfterViewInit
{
  static final log = new Logger('VexflowAngular2');
  static bool debug = false;
  static String js_src =
    "packages/vexflow_angular/js/vexflow-${!debug ? 'min' : 'debug'}.js";
  static bool isLoaded = false;
  static bool inDuring = false;
  static Completer compLoad = new Completer();

  static ScriptElement loadJs() {
    ScriptElement script = new ScriptElement();
    script.async = false;
    script.src = js_src;
    script.type = "text/javascript";
    return script;
  }

  static Future initJsPart() {
    if(!isLoaded) {
      if(!inDuring) {
        inDuring = true;
        ScriptElement script = VexflowAngular.loadJs()..onLoad.listen((_){
          compLoad.complete();
          isLoaded = true;
          inDuring = false;
        });
        document.body.append(script);
      }
      return compLoad.future;
    } else return new Future.value();
  }

  Completer _initCompl = new Completer();
  List<StreamSubscription> _subscriptions = new List();
  List<draw.IDrawer> drawers = [];

  ElementRef _host;
  Element get host => _host.nativeElement;
  CanvasElement buffer;
  CanvasRenderingContext2D bufferCtx;

  Vex.IRenderContext renderContext;
  Vex.Renderer renderer;

  int get devicePixelRatio => window?.devicePixelRatio ?? 1;

  Future get onReady => _initCompl.future;
  int get width {
    final int hostWidth = host.clientWidth > 0 ? host.clientWidth : window.innerWidth;
    final num ret = stavesWidth == 0 ? hostWidth - margin : stavesWidth - margin;
    return (ret / scale).ceil();
  }

  CanvasElement get container => (_host.nativeElement as Element)
    .querySelector('#container');

  CanvasElement get cursorContainer => (_host.nativeElement as Element)
    .querySelector('#cursor');

  CanvasElement get hightlightContainer => (_host.nativeElement as Element)
    .querySelector('#hightlight');

  DivElement get mainCntr => host.querySelector('div');
  int get canvasHeightInt => container.client.height;
  int get canvasWidthInt => container.client.width;
  String get canvasWidth => container.client.width.toString();
  String get canvasHeight => container.client.height.toString();

  DivElement get scroller => host.querySelector('.scroll');

  @Input('disable-check-size') int isDisableCheckSize = false;
  @Input('margin') String marginVal = "25";
  @Input('cursor-img') String cursorImg = "";
  @Input('width-note') int widthNote = 50;
  @Input('line-width') int lineWidth = 1;
  @Input('spacing-between-lines') int spacingBetweenLines = 10;
  @Input('line-fill-style') String lineFillStyle = "#000000";
  @Input('shift-note') int shiftNote = 0;
  @Input() int forceStaveHeight;
  @Input() bool isAbsolute = true;
  @Input() int extWidth;
  @Input() int extHeight;
  @Input() set glyphFontScale(double val)
  {
    _glyphFontScale = val;
  }

  @Input('cssStyle') Map<String, String> cssStyles = <String, String>{};

  @Output() Em.EventEmitter<Void> drawEnd = new Em.EventEmitter();
  @Output() Em.EventEmitter<int> scrollPos = new Em.EventEmitter();
  
  int _stavesWidth = 0;
  int get stavesWidth => _stavesWidth;
  @Input('staves-width') set stavesWidth(int val) {
    _stavesWidth = val;
  }

  int offsetYStave = 0;

  final NavigationService navigation;
  final DataService data;
  draw.InstrumentsDrawer drawer;

  @Output() EventEmitter<TPositionData> position = new EventEmitter();
  EventEmitter get instrument => data.instrument;
  EventEmitter get stave => data.stave;

  /*AutoScroll Data*/
  int _stavesHeight = 0;
  bool _isAutoScroll = true;
  bool _isIternalSrc = false;
  /*--------------*/

  /*-- Auto Resize --*/
  int prevHeightReuqst = 0;
  int prevWidthReuqst = 0;
  /*-----------------*/

  bool _inDuringDraw = false;
  bool _wasSkippedDraw = false;
  bool _disableCursor = false;
  double _scaleX = 1.0; double _scaleY = 1.0;
  double _glyphFontScale = 30.0;

  double scale = 1.0;

  VexflowAngular(this._host, this.navigation, this.data) {
    VexflowAngular.initJsPart();
  }

  int get margin => int.parse(marginVal);
  int get currentPosition => navigation.getSelectedPosition().position;

  @override
  int getMargin() => margin;

  @override
  double getScale() => scale;

  @override
  Vex.IRenderContext getRenderContext() => renderContext;

  @override
  int getStaveWidth() => (width).ceil();

  @override
  int getWidthOfNote() => (widthNote).ceil();

  @override
  int getNoteShift() => shiftNote;

  @override
  int getAllStavesHeight() => (container.clientHeight).ceil();

  @override
  double getGlyphFontScale() => _glyphFontScale;

  @override
  String getCursorImage() => cursorImg;

  @override
  bool disableCheckSize() {
    return isDisableCheckSize;
  }

  bool _skipDraw = false;

  setSkipDraw(bool val) => _skipDraw = val;

  setPositionForCurrent(int pos) {
    setPosition(new TPositionData(data.getCurrentInstrument().name, pos));
  }

  setPosition(TPositionData pos) {
    if (_disableCursor) return;
    navigation.setPosition(pos);
  }

  void setScale(double scale)
  {
    this.scale = scale;
    resizeIntuments();
    this.resize();
  }

  @override
  ngOnInit() {
    _initBuffer();
    compLoad.future.then((_) {
      print("vexflow was initialized");
      _initialize();
    });
  }

  @override
  ngAfterViewInit() {
    { /* Scale main context */
      //var ctx = container.context2D;
      //ctx.scale(1 / devicePixelRatio, 1/ devicePixelRatio);
    }
    DivElement scrollEl = host.querySelector('.scroll');
    _subscriptions.add(scrollEl.onScroll.listen((Event evt) {
      scrollPos.emit(scrollEl.scrollTop);
    }));
  }

  @override
  ngOnChanges(Map<String, SimpleChange> changes) {
    if (changes.containsKey('width-note')
        || changes.containsKey('line-width')
        || changes.containsKey('spacing-between-lines')
        || changes.containsKey('glyphFontScale')
       )
    {
    } 
  }

  @override
  ngOnDestroy() {
    _subscriptions.forEach((StreamSubscription sub) => sub.cancel());
  }

  _initBuffer() {
    buffer = document.createElement('canvas');
    bufferCtx = buffer.getContext('2d');
  }

  _initialize()  {
    _initVexflow();
    _initListeners();
    {
      drawer = new draw.InstrumentsDrawer(data, navigation, this);
      drawers.addAll([
        new draw.ClearDraver(renderContext),
        drawer
      ]);
    }
  }

  _initListeners() {
    _subscriptions.add(host.onScroll.listen((Event evt) {
      if (_isIternalSrc) {
        _isIternalSrc = false;
        return;
      }
      setAutoScroll(false);
    }));
    _subscriptions.add(new rx.Observable(window.onResize)
      .debounce(new Duration(milliseconds: 300)).listen((_)
        => host.dispatchEvent(new CustomEvent('resize'))));
    _subscriptions.add(
      new rx.Observable(host.onResize)
        .listen((Event evt){
          evt.stopPropagation();
          evt.preventDefault();
          resize(resizeBuffer: isAbsolute);
    }));
    _subscriptions.add(cursorContainer.onClick.listen((MouseEvent evt) {
      evt.preventDefault();
      evt.stopPropagation();
      new Future(() => handleCord(evt.offset.x, evt.offset.y));
    }));
    _subscriptions.add(navigation.changePosition.listen((ind) {
      setAutoScroll(true);
      drawer.drawCursorOnCurrent();
      position.emit(ind);
    }));
    _subscriptions.add(navigation.hightlightPosition.listen((pos) {
      setAutoScroll(true);
      drawer.hightlightCursorOnCurrent(pos);
    }));
  }

  _initVexflow() {
    { /*Init Zero Style*/
      final int hostWidth = host.clientWidth;
      container.style.height = "0px";
      container.style.width = stavesWidth == 0
        ? "${hostWidth}px" : "${stavesWidth}px";
      cursorContainer.style.height = "0px";
      cursorContainer.style.width = stavesWidth == 0
        ? "${hostWidth}px" : "${stavesWidth}px";
      hightlightContainer.style.height = "0px";
      hightlightContainer.style.width = stavesWidth == 0
        ? "${hostWidth}px" : "${stavesWidth}px";
    }
    
    renderer = new Vex.Renderer(buffer, Vex.RendererBackends.CANVAS);
    renderContext = renderer.getContext();
    if(!_initCompl.isCompleted) {
      _initCompl.complete();
    }
  }

  setScale2(double x, double y) {
    _scaleX = x;
    _scaleY = y;
  }

  setAutoScroll(bool val) {
    _isAutoScroll = val;
  }

  setGlyphFontScale(double val) => _glyphFontScale = val;

  turnOnOffCursor(bool val) 
  {
   // _disableCursor = !val;
    cursorContainer.style.display = (!val) ? 'none' : '';
  }

  @override
  Future<Null> scrollTo({int position: null}) async {
    if (!_isAutoScroll) return;
    _isIternalSrc = true;
    DivElement scroller = host.querySelector('.scroll');
    if (position == null) {
      scroller.scrollTop = getAllStavesHeight();
    } else {
      scroller.scrollTop = position;
    }
  }

  setBufferSize(int _height, int _width) {
    //double scale = 1.0;
    num height = _height * devicePixelRatio * scale;
    num width = _width * devicePixelRatio * scale;
    buffer.style.height = "${height}px";
    buffer.style.width = "${width}px";
    buffer.setAttribute('width', "${(width - margin).ceil()}");
    buffer.setAttribute('height', "${height}");
    bufferCtx.scale(devicePixelRatio * scale, devicePixelRatio * scale);
  }

  @override
  setStaveHeight(int totalHeight) => _stavesHeight = totalHeight;

  void resizeIntuments()
  {
    for (String name in data.instruments)
    {
      var instr = data.getInstrument(name);
      var params = instr.params;
      params.width = width;
      instr.setParams(params);
    }
  }

  @override
  Future resize({int width: null, int height: null, bool resizeBuffer = true}) {
    final int targetWidth = width ?? (extWidth ?? host.clientWidth);
    final int targetHeight = height ?? (extHeight ?? host.clientHeight);

    if (targetWidth == 0 || targetHeight == 0) {
      return new Future.value();
    }

    if (prevHeightReuqst == targetHeight && prevWidthReuqst == targetWidth) {
      return new Future.value();
    } else {
      prevHeightReuqst = targetHeight;
      prevWidthReuqst = targetWidth;
    }

    int scaledWidth = (targetWidth * devicePixelRatio).ceil();
    int scaledHeight = (targetHeight * devicePixelRatio).ceil();

    //scroller.style.height = "${targetHeight * devicePixelRatio * scale}px";

    if (container != null) {
        container.style.height = "${targetHeight}px";
        container.style.width = "${targetWidth}px";
        container.setAttribute('width', "${scaledWidth}");
        container.setAttribute('height', "${scaledHeight}");
    }

    if (!_disableCursor) {
      scheduleMicrotask(() {
        cursorContainer.style.height = "${targetHeight}px";
        cursorContainer.style.width = "${targetWidth}px";
        cursorContainer.setAttribute('width',  "${scaledWidth / scale}");
        cursorContainer.setAttribute('height', "${scaledHeight / scale}");

        _scaleCtx(cursorContainer, (devicePixelRatio * scale).truncate());

        //hightlightContainer
        hightlightContainer.style.height = "${targetHeight}px";
        hightlightContainer.style.width = "${targetWidth}px";
        hightlightContainer.setAttribute('width',  "${scaledWidth / scale}");
        hightlightContainer.setAttribute('height', "${scaledHeight / scale}");

        _scaleCtx(hightlightContainer, (devicePixelRatio).truncate());
      });
    }

    if (resizeBuffer) {
      setBufferSize(targetHeight, targetWidth);
    }

    resizeIntuments();
    
    return reDraw(inAnimationFrame: false);
  }

  _scaleCtx(CanvasElement el, int pixelRatio)
  {
    var ctx = el.context2D;
    ctx.scale(pixelRatio, pixelRatio);
  }

  _drawFromBuffer(){
    CanvasRenderingContext2D renderer = container.getContext('2d');
    renderer.clearRect(0, 0, canvasWidthInt * devicePixelRatio, canvasHeightInt * devicePixelRatio);
    int x = 0;
    int y = 0;
    // if (!isAbsolute) {
    //   final _scaleX = canvasWidthInt / buffer.width;
    //   final _scaleY = canvasHeightInt / buffer.height;
    //   renderer.scale(_scaleX, _scaleY);
    //   if (_scaleX < 1.0) {
    //     x = ((canvasWidthInt - buffer.width).abs() / 2).ceil();
    //   } else {
    //     x = -1;
    //   }
    // }
    renderer.drawImage(buffer, x, y);
  }

  int _animationFrame = 0;
  Future _reDraw()
  {
    if (_skipDraw) { return new Future.value();}

    if (_inDuringDraw){
      _wasSkippedDraw = true;
       return new Future.value();
     }
    _inDuringDraw = true;
    Completer compl = new Completer();

    Future _draw() async {
      if (drawer == null)
      {
        return new Future.value();
      }

      drawer.disableCursor = _disableCursor;
      List waiters = [];
      for (draw.IDrawer el in drawers) {
        waiters.add(el.draw());
      }

      try
      {
        await Future.wait(waiters);
      }
      catch (err, stack)
      {
        print("${err} | ${stack.toString()}");
      }

      window.cancelAnimationFrame(_animationFrame);
      _animationFrame = window.requestAnimationFrame((_)
      { 
        _drawFromBuffer();
        drawEnd.emit({});
      });
      _inDuringDraw = false;
      
    };

    _draw().then((_) {
      if (_wasSkippedDraw) {
        _wasSkippedDraw = false;
        return _draw();
      } else {
        compl.complete();
      }
    });
    return compl.future;
  }

  Future reDraw({bool inAnimationFrame: true}) {
    return _reDraw();
  }

  handleCord(int x, int y) {
    String name;
    int position;

    TPositionData item = drawer.inPosition(x, y);

    if (item != null) {
      name = item.name;
      position = item.position;
    }

    if (name != null && position != null) {
      if (data.getCurrentInstrument().name != name) {
        data.setCurrentInstrument(name);
      }
      setPosition(item);
    }

  }

  bool hasInstrument(String name) {
    return data.hasInstrument(name);
  }

  selectInstrument(String name) {
    data.setCurrentInstrument(name);
  }

  TPositionData getPositonData() => navigation.getSelectedPosition();

  String getImageSnapshot() {
    return buffer.toDataUrl('image/png', 100);
  }

  Future updateView() => reDraw(inAnimationFrame: false);

  int getPositionBuUid(String uid, {String instrument: ''}) {
    Instrument instr = instrument.isEmpty
      ? getCurrentInstrument() : data.getInstrument(instrument);
    return instr.musicVerticals
      .takeWhile((el) => el.Id != uid)
      .fold(0, (int ret, IStaveItem el) => ret + 1);
  }

  int getCursorPositionBuUid(String uid, {String instrument: ''}) {
    Instrument instr = instrument.isEmpty
      ? getCurrentInstrument() : data.getInstrument(instrument);
    return instr.musicVerticals
      .takeWhile((el) => el.Id != uid)
      .fold(0, (int ret, IStaveItem el) => ret + el.expandOffset());
  }

  bool hightlightByUid(String uid, {int offset: 0, String instrument: ''}) {
    Instrument instr = instrument.isEmpty
      ? getCurrentInstrument() : data.getInstrument(instrument);
    int itemIdx = 0;
    IStaveItem el = instr.musicVerticals.firstWhere((IStaveItem el) {
      final isDone = el.Id == uid;
      if (!isDone) {
        itemIdx += el.expandOffset();
      }
      return isDone;
    }, orElse: () => null);

    navigation.setHightlight(drawer.byIndex(itemIdx + offset, instr.name));

    return true;
  }

  clearHightlight()
  {
    drawer.clearHightlight();
  }

  bool setCursorByUid(String uid, {int offset: 0, String instrument: ''}) {
    Instrument instr = instrument.isEmpty
      ? getCurrentInstrument() : data.getInstrument(instrument);
    int itemIdx = 0;
    IStaveItem el = instr.musicVerticals.firstWhere((IStaveItem el) {
      final isDone = el.Id == uid;
      if (!isDone) {
        itemIdx += el.expandOffset();
      }
      return isDone;
    }, orElse: () => null);
    if (el != null) {
      try {
        navigation.setPosition(drawer.byIndex(itemIdx + offset, instr.name));
      } catch (err) {
        navigation.setPosition(new TPositionData(instr.name, itemIdx + offset));
      }
      return true;
    }
    return false;
  }

  setCursorToEnd({String instrument: ''}) {
    Instrument instr = instrument.isEmpty
      ? getCurrentInstrument() : data.getInstrument(instrument);
    final int idx = instr.musicVerticals      
      .fold(0, (int ret, IStaveItem el) => ret + el.expandOffset());
    navigation.setPosition(new TPositionData(instr.name, idx));
  }

  addInstrument(String name, StaveParams params, [bool setAsDefault = true]) {
    params.y += offsetYStave;
    params.width = width;
    params.widthOfNote = widthNote;
    params.line_width = lineWidth;
    params.lineFillStyle = params.lineFillStyle ?? lineFillStyle;
    params.spacing_between_lines_px = spacingBetweenLines;
    data.addInstrument(name, params, setAsDefault: setAsDefault);
    navigation.setPosition(new TPositionData(name, 0));
    reDraw(inAnimationFrame: false);

    var check;
    check = () {
      if (width <= 0)
      {
        new Future.delayed(new Duration(milliseconds: 300), () => check());
      }
      else
      {
        resizeIntuments();
      }
    };

    check();
  }

  Instrument getCurrentInstrument() => data.getCurrentInstrument();

  Future addNotes(IStaveItem note) {
    data.getCurrentInstrument().addVertical(note);
    return reDraw(inAnimationFrame: false);
  }

  Future insertItem(IStaveItem item, int pos) {
    data.getCurrentInstrument().insertVertical(pos, item);
    return reDraw(inAnimationFrame: false);
  }

  _fixStaveHeightFor(IStaveItem item) {
    Vex.Stave target = StaveDrawer.create(data.getCurrentInstrument()
      .params.clone()..timeSignature = null);
    List<StaveItemWrapper> itemWrap = VoiceDrawer
      .toItemWithWidth([item], target, drawerData: this).toList();
    
    itemWrap.forEach((StaveItemWrapper el) {
      if (el.vexItem.getAttribute('type') == "StaveNote") {
        Vex.StaveNote note = el.vexItem;
        final int y = note.getBoundingBox().getY();
        if (y.isNegative) {
          data.getCurrentInstrument().params.y += y.abs() + 5;
        }
      }
    });
  }

  Future setItemToPos(IStaveItem item, int pos) {
    _fixStaveHeightFor(item);
    data.getCurrentInstrument().changeVertical(pos, item);
    return reDraw(inAnimationFrame: false);
  }

  Future replaceItems(Iterable<IStaveItem> items, {int start: 0}) {
    int idx = start;
    try {
      for (IStaveItem el in items) {
        data.getCurrentInstrument().changeVertical(idx++, el);
      }
    } catch (err) {}
    return reDraw(inAnimationFrame: false);
  }

  StaveParams getParams({String name : ''}) {
    if (name.isEmpty) {
      return data.getCurrentInstrument().params.clone();
    } else {
      return data.getInstrument(name).params.clone();
    }
  }

  changeParams(StaveParams params, {String name: ''}) {
    if (name.isEmpty) {
      data.getCurrentInstrument().setParams(params);
    } else {
      data.getInstrument(name).setParams(params);
    }
    reDraw(inAnimationFrame: false);
  }

  Future removeLastVertical() {
    data.getCurrentInstrument().removeLastVertical();
    return reDraw(inAnimationFrame: false);
  }

  Future removeVertical(int indx) {
    data.getCurrentInstrument().removeVertical(indx);
    return reDraw(inAnimationFrame: false);
  }

  Future clearAll() {
    data.getCurrentInstrument().clearAll();
    return reDraw(inAnimationFrame: false);
  }

  Future setNoteOnCurrent(IStaveItem note) {
    int ind = navigation.getSelectedPosition().position;
    data.getCurrentInstrument().changeVertical(ind, note);
    return reDraw(inAnimationFrame: false);
  }
  
}
